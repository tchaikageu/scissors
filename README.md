# Scissors

Nim clone of [classeur](https://github.com/Martoni/Classeur)

Scissors is a tool to merge text files in one then split it to update. Usefull for writing pandoc/markdown doc

# Install

To install *scissors* simply clone project then use nimble install :

```shell

$ git clone https://git.envs.net/tchaikageu/scissors.git
$ cd scissors
$ nimble install
$ scissors -h

Merge and split text files. Version 0.2.1

Usage:
  scissors [options] [markdownFiles ...]

Arguments:
  [markdownFiles ...]
                   Text files to merge, could be markdown or not

Options:
  -h, --help
  -s, --split                Split file given in SPLIT argument, then update matching files
  -m, --merge                Text files to merge, could be markdown or not, whatever.
  -e, --edit                 Edit merged files in $EDITOR and merge them on exit
  -c, --scissors=SCISSORS    Scissors characters used (default: ✂✂✂)
  -o, --outputfile=OUTPUTFILE
                             Output filename (default: document.md)
  -p, --purge                Purge merged text file from headers tags
  -v, --version              Show version info and exit

```

Simplify pandoc document edition.

# Use it

## Merge
Merge all your text files with following command :
```shell
$ scissors -m intro.md chapitre1.md pouet.md annexes.md -o my_merged_doc.md
Merging intro.md
Merging chapter1.md
Merging chapter2.md
Merging annexes.md
Merged 4 files in my_merged_doc.md
```

## Split

Split and update files with :

```shell
$ scissors -s my_merged_doc.md
Splitting my_merged_doc.md with scissors «✂✂✂»
Found file intro.md
Found file chapter1.md
Found file chapter2.md
Found file annexes.md
```

## Purge

Purge merged text from headers :

```Shell
$ scissors -p my_merged_doc.md
Purging my_merged_doc.md from header ✂✂✂-tagged
my_merged_doc.md purged in file purged_my_merged_doc.md
```

## Edit

It's possible to use merged file directly in its favorite editor and merge on save-exit with edit option :

```shell
$ EDITOR=vim scissors -e intro.md chapter1.md chapter2.md annexes.md
```
# Tips

## Vim
Highlight cut line (in neovim .config/nvim/init.vim):
```vim
" Highlight rules for scissors
syntax region scissorsCut start=+✂✂✂+ end=+✂✂✂+
highlight scissorsCut ctermbg=yellow ctermfg=black
```


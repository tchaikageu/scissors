import std/envvars
import std/os
import std/re
import std/streams
import std/strformat
import std/tempfiles
import argparse

const
  version {.strdefine.}: string = ""
  defaultScissors = "✂✂✂"
  defaultOutputFile = "document.md"

proc merging(markdownFiles: seq[string], outputFile: string,
    scissors: string) =
  echo fmt"Merging {markdownFiles} with scissors {scissors}"
  let output = newFileStream(outputFile, fmWrite)
  for file in markdownFiles:
    if fileExists(file):
      let input = newFileStream(file, fmRead)
      output.writeLine(fmt"{scissors} {file} {scissors}")
      output.write(input.readAll)
    else:
      stderr.writeLine(fmt"File '{file}' doesn’t exists")
  output.close()
  echo fmt"Merged {len(markdownfiles)} files in {outputFile}"

proc splitting(markdownFile: string, scissors: string) =
  if not fileExists(markdownFile):
    stderr.write(fmt"File {markdownFile} doesn’t exists")
    quit(1)
  let tag = re(fmt"^{scissors} (.*) {scissors}$")
  let input = newFileStream(markdownFile, fmRead)
  var line: string
  var output: FileStream
  while readLine(input, line):
    if line =~ tag:
      echo "Found file: ", matches[0]
      output = newFileStream(matches[0], fmWrite)
    else:
      if not isNil(output):
        output.writeLine(line)
      else:
        raise newException(UsageError, "Format error")
  echo fmt"Splitting {markdownFile} with scissors {scissors}"

proc purging(markdownFile: string, scissors: string) =
  let tag = re(fmt"^{scissors} (.*) {scissors}$")
  if not fileExists(markdownFile):
    stderr.write(fmt"File {markdownFile} doesn’t exists")
    quit(1)
  let
    input = newFileStream(markdownFile, fmRead)
    output = newFileStream(fmt"purged_{markdownFile}", fmWrite)
  var line: string
  echo fmt"Purging {markdownfile} from header {scissors}-tagged"
  while readLine(input, line):
    if find(line, tag) == -1:
      output.writeLine(line)
  output.close()
  input.close()
  echo fmt"{markdownfile} purged in file purged_{markdownfile}"

proc launchEditor(filePath: string) =
  let editor = getEnv("EDITOR")
  if editor == "":
    raise newException(UsageError, "$EDITOR is not set, cannot launch editor.")
  discard execShellCmd(fmt"{editor} {filepath}")

proc main =
  let p = newParser("scissors"):
    help(fmt"Merge and split text files. Version {version}")
    arg("markdownFiles", help = "Text files to merge, could be markdown or not", nargs = -1)
    flag("-s", "--split", help = "Split file given in SPLIT argument, then update matching files")
    flag("-m", "--merge", help = "Text files to merge, could be markdown or not, whatever.")
    flag("-e", "--edit", help = "Edit merged files in $EDITOR and split them on exit")
    option("-c", "--scissors", help = fmt"Scissors characters used",
        default = some(defaultScissors))
    option("-o", "--outputfile", help = fmt"Output filename", default = some(defaultOutputFile))
    flag("-p", "--purge", help = "Purge merged text file from headers tags")
    flag("-v", "--version", help = "Show version info and exit",
        shortcircuit = true)
  try:
    let opts = p.parse()
    let markdownFiles = opts.markdownFiles
    if opts.merge and opts.split:
      raise newException(UsageError, "Can't both merge and split at same time, choose one")
    if opts.edit and (opts.merge or opts.split):
      raise newException(UsageError, "Merge and split are incompatible with edit")
    if len(opts.markdownFiles) == 0:
      raise newException(UsageError, "Please provide file names")
    if opts.split:
      splitting(markdownFiles[0], scissors = opts.scissors)
    elif opts.merge:
      merging(markdownFiles, opts.outputfile, opts.scissors)
    elif opts.purge:
      purging(markdownFiles[0], opts.scissors)
    elif opts.edit:
      let tmpFile = createTempFile(prefix = "scissors-", suffix = "")
      merging(markdownFiles, tmpFile.path, opts.scissors)
      launchEditor(tmpFile.path)
      splitting(tmpFile.path, opts.scissors)
      discard tryRemoveFile(tmpFile.path)
    else:
      raise newException(UsageError, "Do you want to merge (-m) or split (-s) or edit (-e) ?")
  except ShortCircuit as e:
    if e.flag == "argparse_help":
      echo p.help
    elif e.flag == "version":
      stdout.writeLine fmt"Scissors v.{version}"
    quit(0)
  except UsageError:
    stderr.writeLine getCurrentExceptionMsg()
    quit(1)

when isMainModule:
  main()

# Package

version = "0.2.2"
author = "Tchaikageu"
description = "Merge and split text files"
license = "WTFPL"
srcDir = "src"
bin = @["scissors"]

import strformat


let opts = &"--verbose -d:lto -d:version=\"{version}\""


# Dependencies

requires "nim >= 1.6.6"
requires "argparse"

task scissors, "Build for danger":
  exec &"nimble install -d:danger -d:strip --opt:size {opts}"

task debug, "Build for debug":
  exec &"nimble build -d:debug --debugger:native -y --styleCheck:hint {opts}"
